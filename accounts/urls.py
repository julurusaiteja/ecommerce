from .views import RegisterView, LogoutView, ChangePasswordView, UpdateProfileView, LogoutAllView
from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

urlpatterns = [
    path('login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('register/', RegisterView.as_view(), name='auth_register'),
    path('changepassword/<int:pk>/', ChangePasswordView.as_view(), name='auth_change_password'),
    path('updateprofile/<int:pk>/', UpdateProfileView.as_view(), name='auth_update_profile'),
    path('logout/', LogoutView.as_view(), name='auth_logout'),
    path('logoutall/', LogoutAllView.as_view(), name='auth_logout_all'),
]
