import Navbar from './components/Navbar';
import './App.css';
import Products from './components/Products'
function App() {
  return (
    <div className="App">
      <Navbar />
    
      <Products />
    </div>
  );
}

export default App;
