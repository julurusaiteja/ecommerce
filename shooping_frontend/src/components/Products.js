import React from 'react';
import axios from 'axios';


class Products extends React.Component {

    state = {
        details: [],
    }

    componentDidMount() {

        let data;

        axios.get('http://127.0.0.1:8000/api/products/')
            .then(res => {
                data = res.data;
                this.setState({
                    details: data
                });
            })
            .catch(err => {
            })
    }

    render() {
        return (
            <div>
                
                {this.state.details.map((detail, id) => (
                        <div key={id}>
                            <div>
                                <div>
                                    <h1>{detail.Title} </h1>
                                    <h4>{detail.Description}</h4>
                                    <img style={{width: 175, height: 175}} className='tc br3'
                                         src={detail.Imagelink} alt='img' />
                                    <h4>Rs {detail.Price}</h4>
                                    <button className="btn btn-outline-primary">ADDTOCART</button>
                                </div><hr />
                            </div>
                        </div>
                    )
                )}
                            </div>
        );
    }
}

export default Products;