import React from 'react';
class Navbar extends React.Component { 
    render(){
        return(
            <div>

        
                    <nav class="navbar navbar-expand-md navbar-light">

                    <a class="navbar-brand" href="#!">
                        <img src="https://content.mystore.com/wp-content/uploads/2018/12/ms-logo-no-padding.png" height="30" alt="mdb logo"></img>
                    </a>

                    
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav11"
                        aria-controls="basicExampleNav11" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>


                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a class="waves-effect" href="#!">Home</a></li>
                       
                    </ol>

                    
                    <div class="collapse navbar-collapse" id="basicExampleNav11">

                        
                        <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <form>
                                <input type='text' placeholder='search product' />
                        
                            </form>
                        </li>
                        <li class="nav-item">
                            <button className='ml-1'> SEARCH</button>
                        </li>
                        <li class="nav-item ml-2">
                            <a href="#!" class="nav-link navbar-link-2 waves-effect">
                            <span class="badge badge-pill red"></span>
                            <i class="fas fa-shopping-cart pl-0">cart</i>
                            </a>
                        </li>
                        
                        <li class="nav-item ml-2">
                            <a href="#!" class="nav-link waves-effect">
                            Contact
                            </a>
                        </li>
                        <li class="nav-item ml-2">
                            <a href="#!" class="nav-link waves-effect">
                            Sign in
                            </a>
                        </li>
                        <li class="nav-item pl-2 mb-2 mb-md-0">
                            <a href="/Register" type="button"
                            class="btn btn-outline-info btn-md btn-rounded btn-navbar waves-effect waves-light">Sign up</a>
                        </li>
                        </ul>

                    </div>
                    

                    </nav>

</div>

        )
    }
}   
export default Navbar;
