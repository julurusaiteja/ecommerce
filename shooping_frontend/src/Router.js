import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'; 
import Register from './auth/Register'
const Routes = () => { 
    return ( 
        <Router> 
            <div> 
                <div>
                <Link to ='./Register' className='p-3 btn-outline-success' >Register</Link>
                </div>
                <Switch>
                    <Route path='/Register'><Register/></Route>
                    
                </Switch>
            </div> 
        </Router> 
    ); 
} 
export default Routes; 