from django.urls import path

from .views import orders_list,orders_details,orderitems_list,orderitems_details
from .productsviews import product_list,product_details,products_search

urlpatterns = [
    path('product/search/',products_search.as_view(),name='productsearch'),
    #path('products/<int:pageno>/',product_list.as_view({'get':'list'}),name='productbypage'),
    path('products/', product_list.as_view({'get': 'list'}), name='productlist'),
    path('product/<int:pk>/',
         product_details.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='productdetails'),
    path('orders/', orders_list.as_view({'get': 'list'}), name='orderlist'),
    path('orders/<str:pk>/',
         orders_details.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='orderdetails'),
    path('orderitem/', orderitems_list.as_view({'get': 'list'}), name='orderitemlist'),
    path('orderitem/<str:pk>',
         orderitems_details.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='orderitemdetails'),

]
