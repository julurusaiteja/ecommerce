from rest_framework import  permissions
from rest_framework.exceptions import PermissionDenied

from .models import Orders, OrdersItems
from .serializers import OrderSerializer, OrderItemsSerializer
from rest_framework import viewsets


# ------------------orders_view-------------------------
class IsUser(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return obj.UserId == request.user


class orders_list(viewsets.ReadOnlyModelViewSet):
    #permission_classes = [IsAuthenticated]
    permission_classes = (IsUser,)
    serializer_class = OrderSerializer

    def get_queryset(self):
        user=self.request.user
        if user.is_authenticated:
            return Orders.objects.filter(User_id=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(User_id=self.request.user)


class orders_details(viewsets.ModelViewSet):

    serializer_class = OrderSerializer
    permission_classes = (IsUser,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return Orders.objects.filter(User_id=user)

        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(User_id=self.request.user)




class orderitems_list(viewsets.ReadOnlyModelViewSet):
    serializer_class = OrderItemsSerializer
    permission_classes = (IsUser,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return OrdersItems.objects.filter(User_id=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class orderitems_details(viewsets.ModelViewSet):

    serializer_class = OrderItemsSerializer
    permission_classes = (IsUser,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return OrdersItems.objects.filter(User_id=user)
        raise PermissionDenied()


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)