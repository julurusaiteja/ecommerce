from django.db import models
from django.contrib.auth.models import User

from django.db.models.deletion import CASCADE


class Products(models.Model):
    Title = models.CharField(max_length=150)
    Description = models.TextField()
    Imagelink = models.FileField(upload_to='photos')
    Price = models.FloatField()
    Created_at = models.DateField(auto_now_add=True)
    Updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return f'{self.id} - {self.Title}  - {self.Price} - {self.Created_at}'


class Orders(models.Model):
    PAYMENT_CHOICE = (
        ('Paytm', 'Paytm_payment_mode'),
        ('Cash', 'Cash_payment_mode'),
        ('Card', 'Card_payment_mode'),
    )

    Orders_CHOICES = (
        ('new', 'new_product'),
        ('paid', 'paid_product')
    )
    User_id = models.ForeignKey(User, on_delete=CASCADE)
    Total = models.FloatField()
    Created_at = models.DateField(auto_now_add=True)
    Updated_at = models.DateField(auto_now=True)
    Status = models.CharField(choices=Orders_CHOICES, max_length=4)
    mode_of_payment = models.CharField(choices=PAYMENT_CHOICE, max_length=20)
    Product_id=models.ForeignKey(Products,on_delete=CASCADE)
    def __str__(self):
        return f'{self.id} - {self.User_id} -{self.Product_id.Title}'


class OrdersItems(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=True)
    Order_id = models.ForeignKey(Orders, on_delete=models.DO_NOTHING)
    Product_id = models.ForeignKey(Products, on_delete=models.DO_NOTHING)
    Quantity = models.PositiveIntegerField(default=1)
    price = models.FloatField()
    User_id=models.ForeignKey(User,on_delete=CASCADE)

    def __str__(self):
        return f"{self.Quantity} - {self.Product_id.Title}"


