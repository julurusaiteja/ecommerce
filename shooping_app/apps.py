from django.apps import AppConfig


class ShoopingAppConfig(AppConfig):
    name = 'shooping_app'
