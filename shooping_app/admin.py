from django.contrib import admin
from .models import Products,Orders,OrdersItems
# Register your models here
class ProductAdmin(admin.ModelAdmin):
    class Meta:
        model=Products
        fields='__all__'
admin.site.register(Products,ProductAdmin)
class OrderAdmin(admin.ModelAdmin):
    class Meta:
        model=Orders
        fields='__all__'
admin.site.register(Orders,OrderAdmin)
class OrdersIteamAdmin(admin.ModelAdmin):
    class Meta:
        model=OrdersItems
        fields='__all__'
admin.site.register(OrdersItems,OrdersIteamAdmin)