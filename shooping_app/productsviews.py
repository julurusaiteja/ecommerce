from rest_framework import viewsets, generics, filters
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny
from shooping_app.models import Products
from shooping_app.serializers import ProductSerializer


# class product_pagination(PageNumberPagination):
#     page_size = 1


class product_list(viewsets.ReadOnlyModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [AllowAny]
    #pagination_class = product_pagination


class product_details(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [AllowAny]


class products_search(generics.ListAPIView):
    search_fields = ['Title']
    filter_backends = (filters.SearchFilter,)
    queryset = Products.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [AllowAny]